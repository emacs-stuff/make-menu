;;; make-menu.el --- Call Makefile targets with ido and display a menu.

;; Copyright (C) 2014-2015 vindarel

;; Author: vindarel <ehvince@mailz.org>
;; Keywords: makefile

;; This file is NOT part of GNU Emacs.

;; GNU GPLv3

;;; Code:
(require 'projectile)
(require 'names)

(define-namespace make-menu-

(defun get-make-commands ()
  "Extract the commands from the Makefile."
  (if (file-exists-p (format "%s/Makefile" (projectile-project-root)))
      (progn
        (with-temp-buffer
          (progn
            (insert-file-contents (format "%sMakefile" (projectile-project-root)))
            (save-restriction
              (setq dj-results '())               ; don't use global variables TODO:
              (goto-char 1)
              (let ((case-fold-search nil))
                (setq dj-results '())
                (while (search-forward-regexp "^[a-z0-9-]+" nil t) ;; regexp is weak
                  (progn
                    (if (null dj-results) (setq dj-results (list (match-string 0)))
                      (setq dj-results (cons (match-string 0) dj-results)))

                    ))
                (nreverse dj-results))))))
    '("")  ;; not void or the detached menu would disappear. We could offer an option.
))

(defun make (command)
  "Suggest make commands to run, based on a simple parsing of the Makefile."
  (interactive (list (ido-completing-read "make: " (get-make-commands))))
  (compile (format "cd %s && make %s" (projectile-project-root) command))
)

;; Dynamic menu to list the MAKE commands.
(easy-menu-define -make-menu global-map "Make"
  '("Make"))

(defun -get-menu ()
  (easy-menu-create-menu
   "Make"
   (mapcar (lambda (command)
             (vector  command
                     `(lambda ()
                        (interactive)
                        (compile (format "cd %s && make %s" (projectile-project-root) ,command)) t)))
           (get-make-commands))))

) ;; end namespace

;; make-menu.el ends here
